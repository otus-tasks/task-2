﻿using DeliveryFood.DAL.Core;
using Task2.DAL.Contracts;
using Task2.DomainModel;
using Task2.DTOModel.Product;

namespace Task2.DAL
{
    public class ProductRepository : BaseRepository<Product, int>, IProductRepository
    {
        public ProductRepository(EbayContext dbContext) : base(dbContext) { }

        public async Task AddAsync(ProductDto modelDto)
        {

            var product = new Product
            {
                Name = modelDto.Name,
                Price = modelDto.Price,
                CategoryId = modelDto.CategoryId
            };

            await AddAsync(product);
        }

        public async Task<List<IdModelProductDto>> GetAllAsync()
        {
            var data = await ListAllAsync();
            return data.Select(x => new IdModelProductDto
            {
                Id = x.Id,
                Name = x.Name,
                Price = x.Price,
                CategoryId = x.CategoryId
            }).ToList();
        }
    }
}
