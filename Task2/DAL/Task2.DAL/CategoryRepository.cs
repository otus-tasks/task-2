﻿using DeliveryFood.DAL.Core;
using Task2.DAL.Contracts;
using Task2.DomainModel;
using Task2.DTOModel;
using Task2.DTOModel.Category;

namespace Task2.DAL
{
    public class CategoryRepository : BaseRepository<Category, int>, ICategoryRepository
    {
        public CategoryRepository(EbayContext dbContext) : base(dbContext) { }

        public async Task AddAsync(NameModelDto modelDto)
        {
            await AddAsync(new Category { Name = modelDto.Name });
        }

        public async Task<List<CategoryDto>> GetAllAsync()
        {
            var data = await ListAllAsync();
            return data.Select(x => new CategoryDto { Id = x.Id, Name = x.Name }).ToList();
        }
    }
}
