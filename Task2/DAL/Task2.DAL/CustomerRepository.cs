﻿using DeliveryFood.DAL.Core;
using Task2.DAL.Contracts;
using Task2.DomainModel;
using Task2.DTOModel.Customer;

namespace Task2.DAL
{
    public class CustomerRepository : BaseRepository<Customer, int>, ICustomerRepository
    {
        public CustomerRepository(EbayContext dbContext) : base(dbContext) { }

        public async Task AddAsync(CustomerDto modelDto)
        {
            var customer = new Customer
            {
                FirstName = modelDto.FirstName,
                LastName = modelDto.LastName,
                Address = modelDto.Address,
                City = modelDto.City,
                PhoneNumber = modelDto.PhoneNumber
            };

            await AddAsync(customer);
        }

        public async Task<List<IdModelCustomerDto>> GetAllAsync()
        {
            var data = await ListAllAsync();

            return data.Select(x => new IdModelCustomerDto
            {
                Id = x.Id, 
                FirstName = x.FirstName,
                LastName= x.LastName,
                City = x.City,
                Address = x.Address,
                PhoneNumber = x.PhoneNumber
            }).ToList();
        }
    }
}
