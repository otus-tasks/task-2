﻿using Task2.DTOModel;
using Task2.DTOModel.Category;

namespace Task2.DAL.Contracts
{
    public interface ICategoryRepository
    {
        Task<List<CategoryDto>> GetAllAsync();
        Task AddAsync(NameModelDto modelDto);
    }
}
