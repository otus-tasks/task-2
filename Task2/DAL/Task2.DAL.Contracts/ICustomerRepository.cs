﻿using Task2.DTOModel.Customer;

namespace Task2.DAL.Contracts
{
    public interface ICustomerRepository
    {
        Task<List<IdModelCustomerDto>> GetAllAsync();
        Task AddAsync(CustomerDto modelDto);
    }
}
