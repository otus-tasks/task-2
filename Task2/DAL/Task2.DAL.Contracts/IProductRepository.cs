﻿using Task2.DTOModel.Product;

namespace Task2.DAL.Contracts
{
    public interface IProductRepository
    {
        Task<List<IdModelProductDto>> GetAllAsync();
        Task AddAsync(ProductDto modelDto);
    }
}
