﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Task2.DomainModel;

namespace DeliveryFood.DAL.Core
{
    public class EbayContext : DbContext
    {
        public EbayContext(DbContextOptions<EbayContext> options)
            : base(options)
        {
        }

        #region Set context entity
        public DbSet<Customer> Customer { get; set; }

        public DbSet<Product> Product { get; set; }

        public DbSet<Category> Category { get; set; }

        public DbSet<Order> Order { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Customer>(ConfigureCustomer);
            builder.Entity<Product>(ConfigureProduct);
            builder.Entity<Category>(ConfigureCategory);
            builder.Entity<Order>(ConfigureOrder);
        }

        private void ConfigureCustomer(EntityTypeBuilder<Customer> builder)
        {
            builder.HasKey(x => x.Id);
        }

        private void ConfigureProduct(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(x => x.Id);

            builder
              .HasOne(n => n.Category)
              .WithMany(b => b.Products)
              .HasForeignKey(b => b.CategoryId);

            builder
             .Property(b => b.CreatedDate)
             .HasDefaultValueSql("NOW()");
        }

        private void ConfigureCategory(EntityTypeBuilder<Category> builder)
        {
            builder.HasKey(x => x.Id);
        }

        private void ConfigureOrder(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(x => x.Id);

            builder
              .HasOne(n => n.Customer)
              .WithMany(b => b.Orders)
              .HasForeignKey(b => b.CustomerId);

            builder
              .HasOne(n => n.Product)
              .WithMany(b => b.Orders)
              .HasForeignKey(b => b.ProductId);
        }
    }

    public class EbayContextFactory : IDesignTimeDbContextFactory<EbayContext>
    {
        private const string DbConnection = "Host=localhost;Port=5432;Database=ebay-db;Username=postgres;Password=4444";

        public EbayContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<EbayContext>();
            optionsBuilder.UseNpgsql(DbConnection);
            return new EbayContext(optionsBuilder.Options);
        }
    }
}
