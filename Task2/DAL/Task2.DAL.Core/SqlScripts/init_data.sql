INSERT INTO public."Category" ("Name")
VALUES 
	('Computers, Tablets & Network Hardware'),
	('Cell Phones, Smart Watches & Accessories'),
	('Video Games & Consoles'),
	('Portable Audio & Headphones'),
	('Virtual Reality Headsets, Parts & Accessories');
	
INSERT INTO public."Product"("Name", "Price", "CategoryId")
	VALUES 
		('Lenovo IdeaPad Gaming 3 15.6', 800, 1),
		('Apple iPhone 11 Pro Max 256GB', 650, 2),
		('Xbox Wireless Controller Robot White', 60, 3),
		('BeyerDynamic DT 990 PRO Studio Headphones', 150, 4),
		('VR Headset with Wireless Bluetooth Controller', 800, 5);

INSERT INTO public."Customer"("FirstName", "LastName", "City", "Address", "PhoneNumber")
	VALUES 
		('Vasia', 'Pupkin', 'City1', 'Address1', 'PhoneNumber1'),
		('Gosha', 'Kalosha', 'City2', 'Address2', 'PhoneNumber2'),
		('Radik', 'Padik', 'City3', 'Address3', 'PhoneNumber3'),
		('Max', 'Tax', 'City4', 'Address4', 'PhoneNumber4'),
		('Lol', 'Prokol', 'City5', 'Address5', 'PhoneNumber5');
		
		
INSERT INTO public."Order"("CustomerId", "ProductId")
	VALUES 
		(1, 1),
		(2, 2),
		(3, 3),
		(4, 4),
		(5, 5);
	
