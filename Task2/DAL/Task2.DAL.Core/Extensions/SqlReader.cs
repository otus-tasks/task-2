﻿namespace Task2.DAL.Core.Extensions
{
    public class SqlReader
    {
        public static string GetSqlFromFile(string fileName)
        {
            string result = string.Empty;
            var path = Path.Combine(AppContext.BaseDirectory, $"SqlScripts/{fileName}");
            Console.WriteLine(path);
            using (StreamReader reader = new(path, System.Text.Encoding.UTF8))
            {
                result = reader.ReadToEnd();
            }

            Console.WriteLine(result);
            return result;
        }
    }
}
