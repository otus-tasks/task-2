﻿using Task2.BLL.Contracts.UI;
using Task2.BLL.Helper;
using Task2.DTOModel.Category;

namespace Task2.BLL.UI
{
    public class CategoryRenderStrategy : ConsolePrintService, IRenderStrategy
    {
        private readonly List<CategoryDto> _categories;
        private const string Id = "Id";
        private const string Name = "Name";

        public CategoryRenderStrategy() { }

        public CategoryRenderStrategy(List<CategoryDto> categories)
        {
            _categories = categories;
        }

        public T AddRow<T>() where T: class, new()
        {
            var result = new T();

            Console.WriteLine("Enter name of category:");
            string name = Console.ReadLine();
            ReflectionHelper.SetValueToProperty<T>(Name, name, result);

            return result;
        }

        public void ShowTable()
        {
            Console.Clear();
            PrintLine();
            PrintRow(Id, Name);
            PrintLine();
            foreach (var category in _categories)
            {
                PrintRow(category.Id.ToString(), category.Name);
            }
            PrintLine();
        }
    }
}
