﻿using Task2.BLL.Contracts.UI;
using Task2.BLL.Helper;
using Task2.DTOModel.Customer;

namespace Task2.BLL.UI
{
    public class CustomerRenderStrategy : ConsolePrintService, IRenderStrategy
    {
        private readonly List<IdModelCustomerDto> _customers;
        private const string Id = "Id";
        private const string FirstName = "FirstName";
        private const string LastName = "LastName";
        private const string City = "City";
        private const string Address = "Address";
        private const string PhoneNumber = "PhoneNumber";

        public CustomerRenderStrategy() { }

        public CustomerRenderStrategy(List<IdModelCustomerDto> customerDtos)
        {
            _customers = customerDtos;
        }

        public T AddRow<T>() where T : class, new()
        {
            var result = new T();

            Console.WriteLine("Enter firstname of user:");
            string firstName = Console.ReadLine();
            ReflectionHelper.SetValueToProperty<T>(FirstName, firstName, result);

            Console.WriteLine("Enter lastname of user:");
            string lastName = Console.ReadLine();
            ReflectionHelper.SetValueToProperty<T>(LastName, lastName, result);

            Console.WriteLine("Enter city of user:");
            string city = Console.ReadLine();
            ReflectionHelper.SetValueToProperty<T>(City, city, result);

            Console.WriteLine("Enter address of user:");
            string address = Console.ReadLine();
            ReflectionHelper.SetValueToProperty<T>(Address, address, result);

            Console.WriteLine("Enter phone number of user:");
            string phoneNumber = Console.ReadLine();
            ReflectionHelper.SetValueToProperty<T>(PhoneNumber, phoneNumber, result);

            return result;
        }

        public void ShowTable()
        {
            Console.Clear();
            PrintLine();
            PrintRow(Id, FirstName, LastName, City, Address, PhoneNumber);
            PrintLine();
            foreach (var category in _customers)
            {
                PrintRow(category.Id.ToString(), category.FirstName, category.LastName, category.City, category.Address, category.PhoneNumber);
            }
            PrintLine();
        }
    }
}
