﻿using Task2.BLL.Contracts.UI;
using Task2.BLL.Helper;
using Task2.DTOModel.Product;

namespace Task2.BLL.UI
{
    public class ProductRenderStrategy : ConsolePrintService, IRenderStrategy
    {
        private readonly List<IdModelProductDto> _products;
        private const string Id = "Id";
        private const string Name = "Name";
        private const string Price = "Price";
        private const string CategoryId = "CategoryId";

        public ProductRenderStrategy() { }

        public ProductRenderStrategy(List<IdModelProductDto> products)
        {
            _products = products;
        }

        public T AddRow<T>() where T : class, new()
        {
            var result = new T();

            Console.WriteLine("Enter name of product");
            string name = Console.ReadLine();
            ReflectionHelper.SetValueToProperty<T>(Name, name, result);

            Console.WriteLine("Enter price of product");
            string price = Console.ReadLine();
            ReflectionHelper.SetValueToProperty<T>(Price, price, result);

            Console.WriteLine("Enter category id");
            string categoryId = Console.ReadLine();
            ReflectionHelper.SetValueToProperty<T>(CategoryId, categoryId, result);

            return result;
        }

        public void ShowTable()
        {
            Console.Clear();
            PrintLine();
            PrintRow(Id, Name, Price, CategoryId);
            PrintLine();
            foreach (var category in _products)
            {
                PrintRow(category.Id.ToString(), category.Name, category.Price.ToString(), category.CategoryId.ToString());
            }
            PrintLine();
        }
    }
}
