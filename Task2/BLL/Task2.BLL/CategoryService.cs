﻿using Task2.BLL.Contracts;
using Task2.DAL.Contracts;
using Task2.DTOModel;
using Task2.DTOModel.Category;

namespace Task2.BLL
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task AddAsync(NameModelDto modelDto)
        {
            await _categoryRepository.AddAsync(modelDto);
        }

        public async Task<List<CategoryDto>> GetAllAsync()
        {
           return await _categoryRepository.GetAllAsync();
        }
    }
}
