﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2.BLL.Helper
{
    public class ReflectionHelper
    {
        public static void SetValueToProperty<T>(string propertyName, string propertyValue, object obj)
        {
            Type type = typeof(T);
            var propertyInfo = type.GetProperty(propertyName);

            if (propertyInfo == null) return;
            
            if (propertyInfo.PropertyType.Name != typeof(Nullable<>).Name)
            {
                propertyInfo.SetValue(obj, Convert.ChangeType(propertyValue, propertyInfo.PropertyType));
            }
            
        }
    }
}
