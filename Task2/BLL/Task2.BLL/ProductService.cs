﻿using Task2.BLL.Contracts;
using Task2.DAL.Contracts;
using Task2.DTOModel.Product;

namespace Task2.BLL
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task AddAsync(ProductDto modelDto)
        {
            await _productRepository.AddAsync(modelDto);
        }

        public async Task<List<IdModelProductDto>> GetAllAsync()
        {
           return await _productRepository.GetAllAsync();
        }
    }
}
