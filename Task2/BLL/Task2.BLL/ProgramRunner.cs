﻿using Task2.BLL.Contracts;
using Task2.BLL.Contracts.UI;
using Task2.BLL.UI;
using Task2.DTOModel;
using Task2.DTOModel.Category;
using Task2.DTOModel.Customer;
using Task2.DTOModel.Product;

namespace Task2.BLL
{
    public class ProgramRunner : IProgramRunner
    {
        private readonly ICategoryService _categoryService;
        private readonly ICustomerService _customerService;
        private readonly IProductService _productService;

        public ProgramRunner(
            ICategoryService categoryService,
            ICustomerService customerService,
            IProductService productService)
        {
            _categoryService = categoryService;
            _customerService = customerService;
            _productService = productService;
        }

        public async Task RunAsync()
        {
            IRenderStrategy strategy = null;
            bool isCanContinue = true;
            while (isCanContinue)
            {
                Console.WriteLine(GetProgramText());
                _ = int.TryParse(Console.ReadLine(), out int enteredNumber);

                switch (enteredNumber)
                {
                    case 1:
                        var categories = await _categoryService.GetAllAsync();
                        strategy = new CategoryRenderStrategy(categories);
                        strategy.ShowTable();
                        continue;
                    case 2:
                        var products = await _productService.GetAllAsync();
                        strategy = new ProductRenderStrategy(products);
                        strategy.ShowTable();
                        continue;
                    case 3:
                        var customers = await _customerService.GetAllAsync();
                        strategy = new CustomerRenderStrategy(customers);
                        strategy.ShowTable();
                        continue;
                    case 4:
                        Console.WriteLine("Enter values for category");
                        strategy = new CategoryRenderStrategy();
                        var newCategory = strategy.AddRow<NameModelDto>();
                        await _categoryService.AddAsync(newCategory);
                        Console.WriteLine("Successfully was added new category");
                        continue;
                    case 5:
                        Console.WriteLine("Enter values for product");
                        strategy = new ProductRenderStrategy();
                        var newProduct = strategy.AddRow<ProductDto>();
                        await _productService.AddAsync(newProduct);
                        Console.WriteLine("Successfully was added new product");
                        continue;
                    case 6:
                        Console.WriteLine("Enter values for customer");
                        strategy = new CustomerRenderStrategy();
                        var newCustomer = strategy.AddRow<CustomerDto>();
                        await _customerService.AddAsync(newCustomer);
                        Console.WriteLine("Successfully was added new customer");
                        continue;
                    default:
                        isCanContinue = false;
                        break;
                }
            }
        }

        #region private methods
        private string GetProgramText()
        {
            return "\n" +
                "Hello!\n" +
                "Show table Category - Enter 1\n" +
                "Show table Product - Enter 2\n" +
                "Show table Customer - Enter 3\n" +
                "\n" +
                "Add row to table Category - Enter 4\n" +
                "Add row to table Product - Enter 5\n" +
                "Add row to table table Customer - Enter 6\n" +
                "\n" +
                "Exit - Enter anything\n";
        }

        #endregion
    }
}
