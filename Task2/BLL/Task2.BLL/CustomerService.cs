﻿using Task2.BLL.Contracts;
using Task2.DAL.Contracts;
using Task2.DTOModel.Customer;

namespace Task2.BLL
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public async Task AddAsync(CustomerDto modelDto)
        {
            await _customerRepository.AddAsync(modelDto);
        }

        public async Task<List<IdModelCustomerDto>> GetAllAsync()
        {
            return await _customerRepository.GetAllAsync();
        }
    }
}
