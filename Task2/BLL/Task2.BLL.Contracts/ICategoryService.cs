﻿using Task2.DTOModel;
using Task2.DTOModel.Category;

namespace Task2.BLL.Contracts
{
    public interface ICategoryService
    {
        Task<List<CategoryDto>> GetAllAsync();
        Task AddAsync(NameModelDto modelDto);
    }
}
