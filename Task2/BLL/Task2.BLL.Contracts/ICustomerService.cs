﻿using Task2.DTOModel.Customer;

namespace Task2.BLL.Contracts
{
    public interface ICustomerService
    {
        Task<List<IdModelCustomerDto>> GetAllAsync();
        Task AddAsync(CustomerDto modelDto);
    }
}
