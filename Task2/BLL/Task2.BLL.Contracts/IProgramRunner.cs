﻿namespace Task2.BLL.Contracts
{
    public interface IProgramRunner
    {
        Task RunAsync();
    }
}
