﻿using Task2.DTOModel.Product;

namespace Task2.BLL.Contracts
{
    public interface IProductService
    {
        Task<List<IdModelProductDto>> GetAllAsync();
        Task AddAsync(ProductDto modelDto);
    }
}
