﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2.BLL.Contracts.UI
{
    public interface IRenderStrategy
    {
        void ShowTable();
        T AddRow<T>() where T : class, new();
    }
}
