﻿using DeliveryFood.DAL.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Task2.BLL;
using Task2.BLL.Contracts;
using Task2.DAL;
using Task2.DAL.Contracts;

namespace Task2ConsoleApp
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var configuration = BuildConfiguration();
            var servicesProvider = BuildServices(configuration).CreateScope().ServiceProvider;
            var serviceRunner = servicesProvider.GetService<IProgramRunner>();

            await serviceRunner.RunAsync();
        }

        private static ServiceProvider BuildServices(IConfiguration configuration)
        {
            return new ServiceCollection()
                 .AddDbContext<EbayContext>(options => options.UseNpgsql(configuration.GetConnectionString("EbayConnection")))

                 //Services
                 .AddTransient<ICategoryService, CategoryService>()
                 .AddTransient<ICustomerService, CustomerService>()
                 .AddTransient<IProductService, ProductService>()

                 //Repository
                 .AddTransient<ICategoryRepository, CategoryRepository>()
                 .AddTransient<ICustomerRepository, CustomerRepository>()
                 .AddTransient<IProductRepository, ProductRepository>()

                 .AddTransient<IProgramRunner, ProgramRunner>()
                 .BuildServiceProvider();
        }

        private static IConfiguration BuildConfiguration()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(AppContext.BaseDirectory);
            builder.AddJsonFile("appsettings.json");

            return builder.Build();
        }
    }
}
