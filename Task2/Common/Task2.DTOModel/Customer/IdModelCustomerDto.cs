﻿namespace Task2.DTOModel.Customer
{
    public class IdModelCustomerDto : CustomerDto
    {
        public int Id { get; set; }
    }
}
