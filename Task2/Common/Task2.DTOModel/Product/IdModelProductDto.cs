﻿namespace Task2.DTOModel.Product
{
    public class IdModelProductDto : ProductDto
    {
        public int Id { get; set; }
    }
}
