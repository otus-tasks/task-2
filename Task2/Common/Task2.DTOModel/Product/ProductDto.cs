﻿namespace Task2.DTOModel.Product
{
    public class ProductDto
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int CategoryId { get; set; }
    }
}
