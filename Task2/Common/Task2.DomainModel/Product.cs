﻿namespace Task2.DomainModel
{
    public class Product
    {
        public int Id { get; set; } 
        public string Name { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CategoryId { get; set; }
        
        public Category Category { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
